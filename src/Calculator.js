import React from "react";
import "./Calculator.css";

export default class Calculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = { formula: '' }
        this.numbers = [
            { id: "zero", sign: 0 },
            { id: "one", sign: 1 },
            { id: "two", sign: 2 },
            { id: "three", sign: 3 },
            { id: "four", sign: 4 },
            { id: "five", sign: 5 },
            { id: "six", sign: 6 },
            { id: "seven", sign: 7 },
            { id: "eight", sign: 8 },
            { id: "nine", sign: 9 },
        ]
        this.operators = [
            { id: "add", sign: "+" },
            { id: "subtract", sign: "-" },
            { id: "multiply", sign: "*" },
            { id: "divide", sign: "/" }
        ]

        this.calculate = this.calculate.bind(this);
        this.addNumber = this.addNumber.bind(this);
        this.addOperator = this.addOperator.bind(this);
        this.addDot = this.addDot.bind(this);
        this.reset = this.reset.bind(this);
        this.setStateSynchronous = this.setStateSynchronous.bind(this);
        this.addFirstZero = this.addFirstZero.bind(this);
        this.strBeforeLastNumber = this.strBeforeLastNumber.bind(this);
    }

    async calculate() {
        // eslint-disable-next-line
        await this.setStateSynchronous(state => ({ formula: eval(state.formula).toString() }));
    }

    setStateSynchronous(stateUpdate) {
        return new Promise(resolve => {
            this.setState(stateUpdate, () => resolve());
        });
    }

    async addFirstZero() {
        if (this.state.formula === "") {
            await this.setStateSynchronous(state => ({ formula: "0" }));

        }
    }

    async addNumber(number) {
        if (number === 0) {
            if (this.state.formula.slice(-1) === "0") {
                return
            }
        }
        await this.setStateSynchronous(state => ({ formula: state.formula + number }));
    }

    strAfterLastOperator(str){
        const opRegex = new RegExp('[\\+|\\-|\\/|\\*]', 'g');
        let lastIndex = -1;
        while (opRegex.test(str)){
            lastIndex = opRegex.lastIndex;
        }
        if (lastIndex === -1) { lastIndex = 0 }
        return str.substring(lastIndex);
    }

    strBeforeLastNumber(str) {
        const numRegex = new RegExp('[0-9]', 'g');
        let lastIndex = -1;
        while (numRegex.test(str)) {
            lastIndex = numRegex.lastIndex;
        }
        if (lastIndex === -1) { lastIndex = 0 }
        return str.substring(0, lastIndex);
    }

    async addOperator(operator) {
        this.addFirstZero();
        if (operator === "-") {
            await this.setStateSynchronous(state => ({ formula: state.formula + operator }));
        } else {
            let newFormula = this.strBeforeLastNumber(this.state.formula);
            await this.setStateSynchronous(state => ({ formula: newFormula + operator }));
        }
    }

    async addDot() {
        this.addFirstZero();
        let lastPartFormula = this.strAfterLastOperator(this.state.formula);
        if (lastPartFormula.indexOf('.') === -1){ 
            await this.setStateSynchronous(state => ({ formula: state.formula + "." }));
        }
        
    }

    async reset() {
        await this.setStateSynchronous(state => ({ formula: "" }));
    }


    render() {
        return (
            <div id="calculator">
                <button className="calcButton" id="equals" onClick={() => this.calculate()}>=</button>
                {this.numbers.map(number => <button className="calcButton" id={number.id} onClick={() => this.addNumber(number.sign)} key={number.id}>{number.sign}</button>)}
                {this.operators.map(operator => <button className="calcButton" id={operator.id} onClick={() => this.addOperator(operator.sign)} key={operator.id}>{operator.sign}</button>)}
                <button className="calcButton" id="decimal" onClick={() => this.addDot()}>.</button>
                <button className="calcButton" id="clear" onClick={() => this.reset()}>C</button>
                <div id="display">{this.state.formula === "" ? 0 : this.state.formula}</div>
            </div>
        )
    }
}
